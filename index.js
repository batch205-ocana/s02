//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//Spaghetti Code - When code is not organized enough that it becomes hard to work it.
// Encapsulation - Organize related information (properties) and behavior(methods) to belong to a single entity.

//Encapsulate the following information into 4 student objects using object literals:

//student1
//student2
//student3
//student4

/*
    Mini-Activity

    Update the logout and listGrades methods of student1 to be able to show student1's email with our message and the grades of student1.

*/

// //create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

//actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects


//QUIZ
/*
    1. Spaghetti Code
    2. Curly Braces
    3. Encapsulation
    4. student1.enroll()
    5. True
    6. window
    7. True
    8. True
    9. True
    10. True
*/
//Function Coding
let student1 = {

    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],
    login: ()=>{

        //login should allow our student object to display its own email with our message.
        //What is this in the context of an object method?
        //this, when inside a method, refers to the object where it is in.
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        console.log(total/4)
    },
    willPass : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 85){
            console.log(true);
        } else {
            console.log(false);
        }
    },
    willPassWithHonors : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 90){
            console.log(true);
        } else if(total/4 >= 85 && total/4 < 90){
            console.log(false);
        }
    },
    getHighestGrade : function (){
        console.log(Math.max.apply(Math, this.grades));
    },
    getLowestGrade : function (){
        console.log(Math.min.apply(Math, this.grades));
    }


};

let student2 = {

    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],
    login: ()=>{
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        console.log(total/4)
    },
    willPass : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 85){
            console.log(true);
        } else {
            console.log(false);
        }
    },
    willPassWithHonors : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 90){
            console.log(true);
        } else if(total/4 >= 85 && total/4 < 90){
            console.log(false);
        }
    },
    getHighestGrade : function (){
        console.log(Math.max.apply(Math, this.grades));
    },
    getLowestGrade : function (){
        console.log(Math.min.apply(Math, this.grades));
    }

};

let student3 = {

    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],
    login: ()=>{
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        console.log(total/4)
    },
    willPass : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 85){
            console.log(true);
        } else {
            console.log(false);
        }
    },
    willPassWithHonors : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 90){
            console.log(true);
        } else if(total/4 >= 85 && total/4 < 90){
            console.log(false);
        }
    },
    getHighestGrade : function (){
        console.log(Math.max.apply(Math, this.grades));
    },
    getLowestGrade : function (){
        console.log(Math.min.apply(Math, this.grades));
    }

};

let student4 = {

    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],
    login: ()=>{
        console.log(`${this.email} has logged in`);

    },
    logout: function (){
        console.log(`${this.email} has logged out`);
    },
    listGrades: function (){
        this.grades.forEach(grade => {
            console.log(grade);
        })
    },
    computeAve : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        console.log(total/4)
    },
    willPass : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 85){
            console.log(true);
        } else {
            console.log(false);
        }
    },
    willPassWithHonors : function (){
        let total = 0;
        this.grades.forEach(grade => {
            total += grade;
        });
        if(total/4 >= 90){
            console.log(true);
        } else if(total/4 >= 85 && total/4 < 90){
            console.log(false);
        }
    },
    getHighestGrade : function (){
        console.log(Math.max.apply(Math, this.grades));
    },
    getLowestGrade : function (){
        console.log(Math.min.apply(Math, this.grades));
    }

};